import { Component, AfterViewInit, ElementRef, Renderer2, Input, QueryList, ContentChildren } from '@angular/core';
import { IonContent, IonItem, DomController } from '@ionic/angular';

@Component({
  selector: 'expandable-header',
  templateUrl: './expandable-header.component.html',
  styleUrls: ['./expandable-header.component.scss']
})
export class ExpandableHeaderComponent implements AfterViewInit {

	@ContentChildren(IonItem, {read: ElementRef}) headerItems: QueryList<ElementRef>;
	@Input('scrollArea') scrollArea: IonContent;

	private headerHeight: number = -100;

	constructor(private element: ElementRef, private renderer: Renderer2, private domCtrl: DomController) {

	}

	ngAfterViewInit(){

		this.domCtrl.write(() => {

			this.headerItems.forEach((item) => {
			
				this.renderer.setStyle(item.nativeElement, 'transition', 'opacity 0.5s linear');

			});

		});

		this.scrollArea.ionScroll.subscribe((ev) => {
			this.resizeHeader(ev);
		});

  }

	resizeHeader(ev){

    let newHeaderHeight = 0;
    let itemsToHide = [];
    let itemsToShow = [];

		this.domCtrl.read(() => {

      // Only run once
      if(this.headerHeight === -100){
        this.headerHeight = this.element.nativeElement.clientHeight;
      }
  
      newHeaderHeight = this.headerHeight - ev.detail.scrollTop;
  
      if(newHeaderHeight < 0){
        newHeaderHeight = 0;
      }

			this.headerItems.forEach((item) => {

				let totalHeight = item.nativeElement.offsetTop + item.nativeElement.clientHeight;

				if(totalHeight > newHeaderHeight){
					itemsToHide.push(item);
				} else if (totalHeight <= newHeaderHeight){
					itemsToShow.push(item);
				}

			});			

		});

		this.domCtrl.write(() => {

			this.renderer.setStyle(this.element.nativeElement, 'height', newHeaderHeight + 'px');

			for(let item of itemsToHide){
				this.renderer.setStyle(item.nativeElement, 'opacity', '0');
			}

			for(let item of itemsToShow){
				this.renderer.setStyle(item.nativeElement, 'opacity', '0.7');
			}

		});

	}
  
}